let trainer = {
	name: "Jimmy",
	age: 25,
	pokemon:["Pikachu","Charmander"],
	friends: {
		paldea: ["Jim","Brok","Nurse Joy"],
		kanto:["Misty"]
	},
	talk: function() {
		console.log("Pikachu! I choose you!");
	}
}

console.log(trainer);
console.log("Result of Dot Notation");
console.log(trainer.name);
console.log("Result of Bracket Notation");
console.log(trainer["pokemon"]);
trainer.talk();

function Pokemon(name, level){
			// Properties of the object
			this.pokemonName = name;
			this.pokemonLevel = level;
			this.pokemonHealth = 2 * level;
			this.pokemonAttack = 1.5 * level;


		// Method
		this.tackle = function(targetPokemon) {
			console.log(this.pokemonName + " tackles " + targetPokemon.pokemonName);

			let hpAfterTackle = targetPokemon.pokemonHealth - this.pokemonAttack;
			
			console.log(targetPokemon.pokemonName + " health is now reduced HP " +  hpAfterTackle);
			

				if (hpAfterTackle<=0) {
				targetPokemon.faint();
				}

			targetPokemon.pokemonHealth = hpAfterTackle ;
			console.log(targetPokemon);

		this.faint = function() {
		console.log(this.pokemonName + " fainted!")

		}

	}
}

	let pikachu = new Pokemon("Pikachu", 12);
	console.log(pikachu);

	let gyarados = new Pokemon("Gyarados", 20);
	console.log(gyarados);

	let koraidon = new Pokemon("Koraidon", 50);
	console.log(koraidon);

	pikachu.tackle(gyarados);
	gyarados.tackle(koraidon);
	koraidon.tackle(pikachu);
	

